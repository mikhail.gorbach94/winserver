# Укажите путь к CSV-файлу
$csvFilePath = "C:\path\to\users.csv"

# Импортируйте данные из CSV-файла
$csvData = Import-Csv -Path $csvFilePath

# Обработка каждой строки CSV-файла
foreach ($row in $csvData) {
    $username = $row.Username
    $password = $row.Password
    $fullName = $row.FullName

    # Проверка наличия пользователя в системе
    if (-not (Get-LocalUser -Name $username -ErrorAction SilentlyContinue)) {
        # Создание учетной записи пользователя
        $securePassword = ConvertTo-SecureString $password -AsPlainText -Force
        $user = New-LocalUser -Name $username -Password $securePassword -FullName $fullName -Description "Created from CSV" -PasswordNeverExpires -AccountNeverExpires

        # Добавление пользователя в группу "Пользователи"
        Add-LocalGroupMember -Group "Пользователи" -Member $user

        Write-Host "Пользователь $username создан и добавлен в группу 'Пользователи'"
    } else {
        Write-Host "Пользователь $username уже существует"
    }
}
